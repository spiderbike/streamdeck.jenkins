﻿using System;
using System.Collections.Generic;
using System.Text;

namespace streamdeck.jenkins
{
    public class JenkinsResponseModel
    {
        public string _class { get; set; }
        public Build[] builds { get; set; }
    }

    public class Build
    {
        public string _class { get; set; }
        public string result { get; set; }
    }

}
