﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using BarRaider.SdTools;
using Narochno.Jenkins;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace streamdeck.jenkins.Actions
{
    [PluginActionId("streamdeck.jenkins.jenkinsstatus")]
    class JenkinsStatus : PluginBase
    {
        private readonly PluginSettings _settings;
        private DateTime _lastCheck = DateTime.MinValue;
        private CheckerBoard checkerBoard;
        private HttpClient _client;



        public JenkinsStatus(SDConnection connection, InitialPayload payload) : base(connection, payload)
        {
            checkerBoard = new CheckerBoard("#336699");
            if (payload.Settings == null || payload.Settings.Count == 0)
            {
                _settings = PluginSettings.CreateDefaultSettings();
            }
            else
            {
                _settings = payload.Settings.ToObject<PluginSettings>();
            }

            _client = new HttpClient();

            var byteArray = Encoding.ASCII.GetBytes($"{_settings.JenkinsUserName}:{_settings.JenkinsApiToken}");
            Logger.Instance.LogMessage(TracingLevel.INFO, $"Updating the HttpClient");

            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        }

        public override void KeyPressed(KeyPayload payload)
        {
            Process.Start((ProcessStartInfo) new()
            {
                UseShellExecute = true,
                FileName = _settings.JenkinsUrl.TrimEnd('/'),
            });
        }

        public override void KeyReleased(KeyPayload payload)
        {
           // throw new NotImplementedException();
        }

        public override void ReceivedSettings(ReceivedSettingsPayload payload)
        {
            Tools.AutoPopulateSettings(_settings, payload.Settings);
        }

        public override void ReceivedGlobalSettings(ReceivedGlobalSettingsPayload payload)
        {
        }

        public override async void OnTick()
        {
            if (_lastCheck.AddSeconds(300) < DateTime.UtcNow)
            {
                try
                {
                    var checkerBitmap = await GetRecentJobStatus();
                    await DrawPullRequestTile(checkerBitmap);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Logger.Instance.LogMessage(TracingLevel.ERROR, $"Error drawing data {ex}");
                }
                _lastCheck = DateTime.UtcNow;
            }
        }

        private async Task<Bitmap> GetRecentJobStatus()
        {

            var results = await GetJenkinsBuildHistory();

            double cols = _settings.Cols;
            double rows = _settings.Rows;

            Logger.Instance.LogMessage(TracingLevel.INFO, $"Using Master branch: {results.UsingMasterBranch}");

            var bitmap = checkerBoard.CreateCheckerBoardBmp(rows, cols,results);
            return bitmap;
        }

        private async Task<JenkinsJobDetails> GetJenkinsBuildHistory()
        {
            Logger.Instance.LogMessage(TracingLevel.INFO, "Going to get jenkins data......");
            var output = new JenkinsJobDetails();
            try
            {
                var url = _settings.JenkinsUrl.TrimEnd('/');
                var dataUrl = $"{url}/api/json?tree=builds[result]";
                var responseText = await _client.GetStringAsync(dataUrl);
                var jenkinsResponse = JsonConvert.DeserializeObject<JenkinsResponseModel>(responseText);
                var usingMaster = await UsingMasterBranch(url);
                output.UsingMasterBranch = usingMaster;
                foreach (var build in jenkinsResponse.builds)
                {
                    if (build.result is null)
                    {
                        output.HistoryItems.Add(3);
                    }
                    else if (build.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        output.HistoryItems.Add(1);
                    }
                    else if (build.result.Equals("FAILURE", StringComparison.InvariantCultureIgnoreCase))
                    {
                        output.HistoryItems.Add(2);
                    }
                    else
                    {
                        output.HistoryItems.Add(0);
                    }
                }
                Logger.Instance.LogMessage(TracingLevel.INFO, (string.Join(",", output)));
                return output;
            }
            catch (Exception e)
            {
                Logger.Instance.LogMessage(TracingLevel.ERROR, $"Error getting jenkins data {e}");
                throw;
            }
        }

        private async Task<bool> UsingMasterBranch(string url)
        {
            var dataUrl = $"{url}/config.xml";
            var responseText = await _client.GetStringAsync(dataUrl);
            responseText = Regex.Replace(responseText, @"<\?xml.*?>", "").Trim();




            XElement config;
            try
            {
                config = XElement.Parse(responseText);
            }
            catch (Exception e)
            {
                Logger.Instance.LogMessage(TracingLevel.ERROR, $"xml : {responseText}");
                throw;
            }

            string branchName;

            if (responseText.Contains("<flow-definition"))
            {
                branchName = config.Elements("definition").Elements("scm").Elements("branches").Elements("hudson.plugins.git.BranchSpec").Elements("name").First().Value;
            }
            else
            {
                branchName = config.Elements("scm").Elements("branches").Elements("hudson.plugins.git.BranchSpec").Elements("name").First().Value;
            }

            Logger.Instance.LogMessage(TracingLevel.INFO, $"branchName : {branchName}");
            
            return branchName.Contains("master", StringComparison.InvariantCultureIgnoreCase);
        }

        public override void Dispose()
        {

        }


        private class PluginSettings
        {
            public static PluginSettings CreateDefaultSettings()
            {
                PluginSettings instance = new PluginSettings();
                instance.JenkinsApiToken = string.Empty;
                instance.JenkinsUrl = string.Empty;
                instance.JenkinsUserName = "Username goes here";
                instance.Cols = 5;
                instance.Rows = 7;
                return instance;
            }

            [JsonProperty(PropertyName = "jenkinsUrl")]
            public string JenkinsUrl { get; set; }

            [JsonProperty(PropertyName = "jenkinsUserName")]
            public string JenkinsUserName { get; set; }

            [JsonProperty(PropertyName = "jenkinsApiToken")]
            public string JenkinsApiToken { get; set; }

            [JsonProperty(PropertyName = "cols")]
            public int Cols { get; set; }

            [JsonProperty(PropertyName = "rows")]
            public int Rows { get; set; }
        }

        private async Task DrawPullRequestTile(Bitmap bitmap)
        {
            try
            {
                await Connection.SetImageAsync(bitmap);
            }
            catch (Exception ex)
            {
                Logger.Instance.LogMessage(TracingLevel.ERROR, $"Error drawing data {ex}");
            }
        }

    }

    public class JenkinsJobDetails
    {
        public bool UsingMasterBranch { get; set; }
        public List<int> HistoryItems { get; set; } = new();
    }
}
