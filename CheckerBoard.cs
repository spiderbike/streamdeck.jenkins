﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Text;
using streamdeck.jenkins.Actions;

namespace streamdeck.jenkins
{



    public class CheckerBoard
    {
        private SolidBrush _bgBrush;
        private SolidBrush _fgBrush;
        private SolidBrush _redBrush;
        private SolidBrush _greenBrush;
        private SolidBrush _greyBrush;
        private readonly SolidBrush _blueBrush;
        private readonly SolidBrush _orangeBrush;

        public CheckerBoard(string bgColourHex)
        {
            _bgBrush = new SolidBrush(ColorTranslator.FromHtml(bgColourHex));
            _fgBrush = new SolidBrush(ColorTranslator.FromHtml("#FFFFFF"));
            _redBrush = new SolidBrush(ColorTranslator.FromHtml("#FF0000"));
            _greenBrush = new SolidBrush(ColorTranslator.FromHtml("#00FF00"));
            _greyBrush = new SolidBrush(ColorTranslator.FromHtml("#CCCCCC"));
            _blueBrush = new SolidBrush(ColorTranslator.FromHtml("#336699"));
            _orangeBrush = new SolidBrush(ColorTranslator.FromHtml("#FFA500"));
        }

        public Bitmap CreateCheckerBoardBmp(double rows, double cols, JenkinsJobDetails results)
        {
            var bmp = GenerateGenericKeyImage(out var graphics);
            var height = bmp.Height;
            var width = bmp.Width;

            var fontDefault = new Font("Verdana", 20, FontStyle.Bold);

            int bannerHeight = 40;
            double widthOfItem = (width / cols);
            double heightOfItem = ((height - bannerHeight) / rows);
            int squareNumber = 0;
            //Draw colours
            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < cols; x++)
                {
                    int startX = (int)(widthOfItem * x);
                    int startY = (int)(heightOfItem * y);
                    if (squareNumber >= results.HistoryItems.Count)
                    {
                        graphics.FillRectangle(_greyBrush, startX, startY, (int)widthOfItem, (int)heightOfItem);
                    }
                    else if (results.HistoryItems[squareNumber] == 1)
                    {
                        graphics.FillRectangle(_greenBrush, startX, startY, (int)widthOfItem, (int)heightOfItem);
                    }
                    else if (results.HistoryItems[squareNumber] == 2)
                    {
                        graphics.FillRectangle(_redBrush, startX, startY, (int)widthOfItem, (int)heightOfItem);
                    }
                    else if (results.HistoryItems[squareNumber] == 3)
                    {
                        graphics.FillRectangle(_orangeBrush, startX, startY, (int)widthOfItem, (int)heightOfItem);
                    }
                    else
                    {
                        graphics.FillRectangle(_blueBrush, startX, startY, (int)widthOfItem, (int)heightOfItem);
                    }

                    squareNumber++;
                }

            }


            //Draw grid
            Pen blackPen = new Pen(Color.Black, 1);
            for (int y = 0; y < rows; y++)
            {
                int startY = (int)(heightOfItem * y);
                for (int x = 0; x < cols; x++)
                {
                    int startX = (int)(widthOfItem * x);
                    graphics.DrawLine(blackPen, startX, 0, startX, height);
                    graphics.DrawLine(blackPen, 0, startY, width, startY);
                }
            }

            var labelColour = results.UsingMasterBranch ? _bgBrush : _orangeBrush;
            graphics.FillRectangle(labelColour, 0, (height - bannerHeight), width, height);
            graphics.Dispose();
            return bmp;
        }

        public static Bitmap GenerateGenericKeyImage(out Graphics graphics)
        {
            return GenerateKeyImage(144, 144, out graphics);
        }

        private static Bitmap GenerateKeyImage(int height, int width, out Graphics graphics)
        {

            Bitmap bitmap = new Bitmap(width, height);
            SolidBrush solidBrush = new SolidBrush(Color.Black);
            graphics = Graphics.FromImage((Image)bitmap);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            graphics.FillRectangle((Brush)solidBrush, 0, 0, width, height);
            return bitmap;
        }


    }

}
