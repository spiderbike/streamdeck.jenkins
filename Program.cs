﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using BarRaider.SdTools;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace streamdeck.jenkins
{
    class MainService : IHostedService
    {
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly CommandLineArgs _commandLineArgs;


        public MainService(IHostApplicationLifetime appLifetime, CommandLineArgs commandLineArgs)
        {
            _appLifetime = appLifetime;
            _commandLineArgs = commandLineArgs;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {

            await Console.Out.WriteLineAsync($"{Assembly.GetExecutingAssembly().GetName().Name} {Assembly.GetExecutingAssembly().GetName().Version}");


            try
            {
                SDWrapper.Run(_commandLineArgs.Args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            if (Debugger.IsAttached)
            {
                await Console.Out.WriteLineAsync("press any key to exit.");
                Console.ReadKey();
            }
            _appLifetime.StopApplication();
        }



        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }

    class Program
    {

        static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<ConsoleLifetimeOptions>(options =>
                    {
                        options.SuppressStatusMessages = true;
                    });
                    services.AddSingleton(new CommandLineArgs { Args = args });

                    services.AddSingleton<IHostedService, MainService>();
                });

            await builder.RunConsoleAsync();
        }
    }
    public class CommandLineArgs
    {
        public string[] Args { get; set; }
    }

}
